from pyscript import document, when
from pyodide.ffi import create_proxy
from pyweb import pydom
from random import randrange
from collections import Counter
import json
from functools import partial
from types import SimpleNamespace

node_win = pydom["#win"][0]
SIDE = 8

class TileElement:
    def __init__(self, idn, x, y):
        self.idn = idn
        self.x = x
        self.y = y
    
    @property
    def id_(self):
        return f"{self.idn}_{self.x}_{self.y}"
    
    def js(self):
        l = document.querySelector(".svgClass").getSVGDocument().getElementById(self.id_)
        assert l, f"No element with id `{id_}`"
        return l
    
    def reveal(self):
        self.js().style.visibility = "visible"
        
    def hide(self):
        self.js().style.visibility = "hidden"
        
    def link(self, type_):
        def decorator(func):
            print("added event listener", type_)
            self.js().addEventListener(type_, create_proxy(func))
        return decorator
    
    def set_color(self, colour):
        self.js().setAttribute("fill", colour)
    
class ElementWrapper:
    def __init__(self, idn):
        self.idn = idn
        
    def __getitem__(self, key):
        return TileElement(self.idn, *key)
    
    def all_subs(self):
        return {(x,y): self[x,y] for x in range(8) for y in range(8)}
    
    def link(self, type_):
        def decorator(func):
            for k, v in self.all_subs().items():
                v.link(type_)(partial(func, *k))
        return decorator


SOLDIER = ElementWrapper("soldier")
FLOOR = ElementWrapper("floor")
WALL = SimpleNamespace(E=ElementWrapper("wall_E"), W=ElementWrapper("wall_W"))

soldiers = {}
covered = {}
for xx in range(8):
    for yy in range(8):
        covered[xx, yy] = False
        soldiers[xx, yy] = False
        

for x in range(8):
    for y in range(8):
        SOLDIER[x, y].hide()
        WALL.E[x, y].hide()
        WALL.W[x, y].hide()
        FLOOR[x, y].set_color("white")
        

with open("levels.json") as file:
    levels = json.load(file)
    
for wall in levels["default"]["walls"]:
    x = wall["x"]
    y = wall["y"]
    if wall["d"] == "U":
        WALL.E[x, y].reveal()
    else:
        WALL.W[x, y].reveal()
        
def recalculate_cover():
    for x in range(8):
        for y in range(8):
            covered[x, y] = False
    for x in range(8):
        for y in range(8):
            if soldiers[x, y]:
                for xx, yy in levels["default"]["visible"][f"{x}_{y}"]:
                    covered[xx, yy] = True
                    
    if all(covered.values()):
        node_win._js.showModal()
                
                
        
@FLOOR.link("mouseover")
def hover_tile(x, y, _event=None):
    base_cover()
    for xx, yy in levels["default"]["visible"][f"{x}_{y}"]:
        FLOOR[xx, yy].set_color("beige")
    FLOOR[x, y].set_color("red")
    
def set(x, y):
    if sum(soldiers.values()) < 3 or soldiers[x, y]:
        soldiers[x, y] = not soldiers[x, y]
        if soldiers[x, y]:
            SOLDIER[x, y].reveal()
        else:
            SOLDIER[x, y].hide()
        recalculate_cover()
    
@FLOOR.link("click")
def floor_set(x, y, _event=None):
    return set(x, y)
    
@SOLDIER.link("click")
def soldier_set(x, y, _event=None):
    return set(x, y)
    
def base_cover():
    for xx in range(8):
        for yy in range(8):
            if covered[xx, yy]:
                FLOOR[xx, yy].set_color("#e6ffe6")
            else:
                FLOOR[xx, yy].set_color("white")

@FLOOR.link("mouseout")
def unhover_tile(x, y, _event=None):
    base_cover()
    