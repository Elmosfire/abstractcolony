rm -rf release && echo "removed release" || echo "no release folder, continuing"
python3 -m venv venv || echo "env already exists"
source venv/bin/activate
pip install -r build/requirements.txt
mkdir release
bash build/build.sh
cd release
python3 -m http.server
