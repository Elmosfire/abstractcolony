SIDE = 8

header = """<?xml version="1.0" ?>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:krita="http://krita.org/namespaces/svg/krita" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:xlink="http://www.w3.org/1999/xlink" baseProfile="full" height="750pt" version="1.1" viewBox="0 0 3000 1500" width="1500pt">
	<defs/>
  {}
</svg>
"""

tile = """
	<g id="base_{loc}" transform="scale(3 3) translate({x},{y})">
	<g id="floor_{loc}" fill="white">
	<path d="M 400.0,300.0 L 450.0,325.0 L 400.0,350.0 L 350.0,325.0 L 400.0,300.0" stroke="black"/>
	</g>
	<g transform="translate(380,265) scale(0.07 0.07)" id="soldier_{loc}">
	<path d="M 91.5126,368.69 C 66.8746,269.551 51.0359,203.85 43.9964,171.586 L 0.0,110.871 L 37.8369,0.0 L 222.622,4.39964 L 211.183,157.507 L 162.787,323.814 C 162.787,323.814 139.029,338.773 91.5126,368.69" fill="#999999" fill-rule="evenodd" id="shape0" sodipodi:nodetypes="cccccccc" stroke="#999999" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="translate(124.069937555754, 465.482247992864)"/>
	<path d="M 0.0,0.0 C 34.65,14.85 56.925,37.35 66.825,67.5 C 76.725,97.2 81.675,118.8 81.675,132.3 C 81.675,145.8 71.55,161.1 51.3,178.2 L 24.975,190.35" fill="#999999" fill-rule="evenodd" id="shape0" sodipodi:nodetypes="cczcc" stroke="#000000" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="translate(356.399999269935, 501.524998972655)"/>
	<path d="M 48.4279,0.0 C 23.7899,36.957 22.9099,55.4355 24.6698,79.1936 C 26.4296,102.952 50.1877,146.948 50.1877,178.626 C 50.1877,210.303 37.8687,237.581 27.3096,255.179 C 16.7504,272.778 -0.848149,291.256 0.0317792,322.934 C 0.911708,354.611 1.79164,348.452 17.6304,362.531 C 33.4691,376.609 39.6286,381.009 78.3454,381.009 C 117.062,381.009 126.144,383.649 140.522,366.93 C 150.107,355.784 144.633,339.359 124.102,317.654 C 114.129,311.788 107.97,306.215 105.623,300.936 C 102.104,293.016 98.5838,286.857 110.023,268.378 C 121.462,249.9 129.664,239.341 140.522,225.262 C 147.76,215.876 162.232,198.571 183.937,173.346" fill="#999999" fill-rule="evenodd" id="shape1" sodipodi:nodetypes="cczzzzzzcczzzc" stroke="#000000" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="translate(122.278301051484, 571.073684210526)"/>
	<path d="M 80.3178,328.213 C 113.755,356.664 176.23,364.29 267.743,351.092 C 305.58,317.654 323.178,219.982 323.178,177.746 C 323.178,135.509 324.058,82.7133 301.18,51.9158 C 278.302,21.1183 224.029,0.0 178.571,0.0 C 133.114,0.0 68.8787,14.9588 35.4414,43.1165 C 2.00414,71.2742 3.764,111.751 2.88407,159.267 C 2.00414,206.783 7.28372,245.5 36.3214,276.298 C 55.6798,296.829 71.5185,313.841 83.8375,327.333 C 38.9611,370.89 13.4432,416.206 3.764,451.403 C -5.91521,486.601 6.02564,505.999 6.90557,551.755 C 7.7855,597.512 50.2105,638.462 95.8217,630.049 C 141.433,621.636 75.7934,536.163 69.9393,486.617" fill="#999999" fill-rule="evenodd" id="shape3" sodipodi:nodetypes="cczzzzzzczzzc" stroke="#000000" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="translate(84.2288624620344, 122.310080285459)"/>
	<path d="M 102.37,0.0 C 141.967,93.2724 138.447,224.382 134.048,260.459 C 129.648,296.536 132.288,289.497 115.569,311.495 C 98.8507,333.493 100.611,333.493 101.49,359.011 C 102.37,384.529 125.249,391.568 136.688,389.808 C 148.127,388.049 163.085,400.368 163.965,415.326 C 164.845,430.285 162.206,441.724 127.888,440.844 C 105.01,440.258 75.9725,439.378 40.7754,438.204 L 0.0,414.446" fill="#999999" fill-rule="evenodd" id="shape4" sodipodi:nodetypes="czzzzzzcc" stroke="#000000" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="translate(245.201427297056, 472.521677074041)"/>
	<path d="M 13.6983,0.0 C -0.38051,6.45281 -3.60691,14.0789 4.01913,22.8781 L 7.53885,22.8781" fill="none" id="shape5" sodipodi:nodetypes="ccc" stroke="#000000" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="translate(383.149465969091, 366.050312221231)"/>
	<path cx="16.7860838537021" cy="12.9741302408563" d="M 0.0,12.9741302408563 A 16.7860838537021,12.9741302408563 0.0 1,0 33.5721677074042,12.9741302408563 A 16.7860838537021,12.9741302408563 0.0 1,0 0.0,12.9741302408563" fill="none" id="shape2" rx="16.7860838537021" ry="12.9741302408563" stroke="#0e0e25" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="translate(291.121498661909, 364.290454950937)"/>
	<path d="M 0.0,0.0 L 325.360881721934,0.0 L 325.360881721934,27.947202791118 L 0.0,27.947202791118 L 0.0,0.0" fill="#000000" fill-rule="evenodd" height="27.947202791118" id="shape0" stroke="#000000" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="matrix(0.948628233165087 -0.316392912755782 0.316392912755782 0.948628233165087 170.719139516465 665.165732083203)" width="325.360881721934"/>
	<path d="M 383.649,226.324 C 385.409,-14.7765 233.181,0.182286 189.185,0.182286 C 145.188,0.182286 1.75986,24.8203 0.879929,128.652 C 0.29331,197.873 0.0,230.724 0.0,227.204 C 0.0,227.204 127.883,226.911 383.649,226.324" fill="#007510" fill-rule="evenodd" id="shape0" sodipodi:nodetypes="czzcc" stroke="#000000" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="translate(51.035860838537, 64.0525045930818)"/>
	<path d="M 13.6983,0.0 C -0.380509,6.45281 -3.60691,14.0789 4.01913,22.8781 L 7.53885,22.8781" fill="none" id="shape1" sodipodi:nodetypes="ccc" stroke="#000000" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="translate(384.029393544083, 364.290454950937)"/>
	<path d="M 0.0,59.1435 C 9.45,20.4197 4.725,10.0304 24.975,1.53006 C 38.475,-4.13683 49.275,5.93765 57.375,31.7535" fill="#999999" fill-rule="evenodd" id="shape2" sodipodi:nodetypes="czc" stroke="#000000" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="translate(380.024999221541, 579.40649881312)"/>
	<path cx="16.7860838537021" cy="12.9741302408563" d="M 0.0,12.9741302408563 A 16.7860838537021,12.9741302408563 0.0 1,0 33.5721677074042,12.9741302408563 A 16.7860838537021,12.9741302408563 0.0 1,0 0.0,12.9741302408563" fill="none" id="shape01" rx="16.7860838537021" ry="12.9741302408563" stroke="#0e0e25" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="13.248" transform="translate(291.121498661909, 364.290454950937)"/>
	</g>
	<g id="wall_W_{loc}">
	<path d="M 400.0,350.0 L 350.0,325.0 L 350.0,305.0 L 400.0,330.0 L 400.0,350.0" fill="lightgray" stroke="blue"/>
  </g>
  <g id="wall_E_{loc}">
	<path d="M 450.0,325.0 L 400.0,350.0 L 400.0,330.0 L 450.0,305.0 L 450.0,325.0" fill="lightgray" stroke="blue"/>
	</g>
	</g>
"""

system = []

for x in range(8):
    for y in range(8):
        system.append(
            tile.format(
                loc=f"{x}_{y}", x=50 * (x - y + SIDE) - 350, y=25 * (x + y) - 200
            )
        )

with open("release/board.svg", "w") as f:
    f.write(header.format("\n".join(system)))
