from shapely.geometry import LineString
import cmath
import numpy as np
import json

SIDE = 8

dirs = dict(U=1j, R=1, D=-1j, L=-1)

class Wall:
  def __init__(self, p1, dir):
    self.p1 = p1
    self.p2 = p1 + dirs[dir]

  def __repr__(self):
    return f"Wall({self.p1}, {self.p2})"

  def center(self):
    return (self.p1 + self.p2)/2

  def edges(self):
    F = self.p1 - self.center()
    yield self.center() + F * 1j
    yield self.center() - F * 1j

  def line_shape(self):
    s1, s2 = self.edges()
    return LineString([(s1.real, s1.imag), (s2.real, s2.imag)])

class Ray:
  def __init__(self, p1, p2):
    self.p1 = p1
    self.p2 = p2

  def __repr__(self):
    return f"Ray({self.p1}, {self.p2})"

  def collide(self, wall):
    return wall.line_shape().intersects(self.line_shape())

  def line_shape(self):
    return LineString([(self.p1.real, self.p1.imag), (self.p2.real, self.p2.imag)])


class Layout:
  def __init__(self, walls):
    self.walls = walls

  def ray_valid(self, ray):
    for wall in self.walls:
      if ray.collide(wall):
        return False
    return True

  def get_all_visible(self, loc):
    for target_x in range(SIDE):
      for target_y in range(SIDE):
        target = target_x + target_y*1j
        circle = [cmath.exp(x*1j) for x in np.linspace(0, 2*np.pi, 12)]
        for p in circle:
          bet = loc + p*0.7
          ray1 = Ray(loc, bet )
          ray2 = Ray(bet, target)
          if self.ray_valid(ray1) and self.ray_valid(ray2):
            yield (target_y, target_x)
            break
          
          
with open("release/levels.json") as file:
    LEVELS = json.load(file)
    
    
for k in LEVELS:
    layout = Layout([Wall(1j*w["x"] + w["y"], w["d"]) for w in LEVELS[k]["walls"]])
    
    OUTPUT = {}
    LEVELS[k]["visible"] = OUTPUT
    
    for x in range(8):
        for y in range(8):
            OUTPUT[f"{y}_{x}"] = [list(z) for z in layout.get_all_visible(x+y*1j)]
    
with open("release/levels.json", "w") as file:
    LEVELS = json.dump(LEVELS, file, indent=2)