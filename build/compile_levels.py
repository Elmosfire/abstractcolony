from pathlib import Path
import json

OUTPUT = {}

for filename in Path("build/level_layout").glob("*.txt"):
    with filename.open() as file:
        layout = file.read()
        walls = []
        OUTPUT[filename.stem] = dict(walls=walls)
        for x, line in enumerate(layout.split("\n")):
            if len(line) < 16:
                line += " " * (16 - len(line))
            for y, (a, b) in enumerate(zip(line[::2], line[1::2])):
                if a == "_":
                    walls.append(dict(y=y,x=x,d="U"))
                if b == "|":
                    walls.append(dict(y=y,x=x,d="R"))
                    
                    
with open("release/levels.json", "w") as file:
    json.dump(OUTPUT, file, indent=2)